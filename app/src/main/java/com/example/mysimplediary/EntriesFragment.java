package com.example.mysimplediary;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public class EntriesFragment extends Fragment {

    private List<Entry> entries;
    private Context context;
    private String[] titles;
    private String[] contents;
    private long[] dates;

    public EntriesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_entries, container, false);

        context = this.getActivity();
        entries = EntryDatabaseHelper.getEntries(context);

        Bundle bundle = this.getArguments();
        boolean getAllEntries = bundle.getBoolean("getAllEntries");
        if (getAllEntries){
            titles = new String[entries.size()];
            contents = new String[entries.size()];
            dates = new long[entries.size()];
            for (int i = 0; i < entries.size(); i++) {
                titles[i] = entries.get(i).getTitle();
                contents[i] = entries.get(i).getContent();
                dates[i] = entries.get(i).getDate();
            }
        } else {
            ArrayList<Integer> ids = bundle.getIntegerArrayList("ids");
            titles = new String[ids.size()];
            contents = new String[ids.size()];
            dates = new long[ids.size()];
            for (int i = 0; i < ids.size(); i++) {

                titles[i] = entries.get(ids.get(i)).getTitle();
                contents[i] = entries.get(ids.get(i)).getContent();
                dates[i] = entries.get(ids.get(i)).getDate();
            }
        }

        // RecyclerView
        RecyclerView entryRecycler = (RecyclerView) view.findViewById(R.id.entry_recycler);
        EntryAdapter adapter = new EntryAdapter(titles,contents,dates);
        entryRecycler.setAdapter(adapter);
        LinearLayoutManager manager = new LinearLayoutManager(context);
        entryRecycler.setLayoutManager(manager);

        adapter.setListener(new EntryAdapter.Listener() {
            public void onClick(int position) {
                Intent intent = new Intent(context, EntryDetailsActivity.class);
                intent.putExtra(EntryDetailsActivity.ENTRY_ID, position);
                startActivity(intent);
            }
        });
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getGroupId() == 0) {  //"Edit"
            Intent intent = new Intent(context, NewEntryActivity.class);
            intent.putExtra(NewEntryActivity.ENTRY_ID, item.getItemId());
            startActivity(intent);
        }
        if (item.getGroupId() == 1) { //"Delete"
            new AlertDialog.Builder(context)
                    .setTitle(R.string.delete_dialog_title)
                    .setMessage(R.string.delete_dialog_content)
                    .setPositiveButton(R.string.button_yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            int id = item.getItemId();
                            int del = entries.get(id).getId();
                            EntryDatabaseHelper.deleteEntry(context, del);
                            Intent intent = new Intent(context, MainActivity.class);
                            startActivity(intent);
                        }
                    })
                    // A null listener allows the button to dismiss the dialog and take no further action.
                    .setNegativeButton(R.string.button_no, null)
                    // .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }
        return true;
    }

}
