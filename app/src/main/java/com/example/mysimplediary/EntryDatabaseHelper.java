package com.example.mysimplediary;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class EntryDatabaseHelper extends SQLiteOpenHelper {

    private static final String DB_NAME="diary";
    private static  final int DB_VERSION=1;
    private static List<Entry> entries;

    EntryDatabaseHelper(Context context){
        super(context, DB_NAME, null, DB_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        updateDatabase(db, 0, DB_VERSION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        updateDatabase(db, oldVersion, newVersion);
    }

    private void updateDatabase(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < 1) {
            db.execSQL("CREATE TABLE ENTRY (_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "TITLE TEXT, "
                    + "CONTENT TEXT, "
                    + "DATE INTEGER);");
            insertEntry(db, "Hello",
                    "This is my first note. I'm so excited!",
                    "1552089330885");
            insertEntry(db, "Новый квест",
                    "Новый технологичный квест с нестандартными загадками, превосходными декорациями и захватывающей атмосферой.\n" +
                            "Игра на взаимодействие - новый формат квеста, в котором появилась возможность не просто играть, но и управлять друг другом.\n" +
                            "Проверьте слаженность работы своей команды! Сможете ли вы полностью довериться ей?\n" +
                            "Интересно будет всем, ведь перед началом игры вы сможете выбрать свой комфортный режим для прохождения: облегченный, для опытных или детский.",
                    "1552089330885");
            insertEntry(db, "Areia do mar",
                    "Areia do mar, areia do mar\n" +
                            "o que você tem, para me contar\n" +
                            "Areia do mar, areia do mar\n" +
                            "o que você tem, para me contar\n" +
                            "Onda que quebra na praia\n" +
                            "quebrava no casco do navio\n" +
                            "navio que trouxe de Angola\n" +
                            "os negros para o Brasil\n" +
                            "Areia do mar, areia do mar\n" +
                            "o que você tem, para me contar\n" +
                            "Vagando sóbre o mar\n" +
                            "chegava o tumbeiro\n" +
                            "trazendo negros de batalhá\n" +
                            "de espirito guerreiro\n" +
                            "Areia do mar, areia do mar\n" +
                            "o que você tem, para me contar",
                    "1553081038156");
            insertEntry(db, "Михаил Лабковский о воле",
                    "— Воля — это не кувалда или отбойный молоток, она скорее напоминает сложный инструмент, который нуждается в тонкой настройке. " +
                            "Чрезмерные усилия и перегрузки быстро сломают его, и вам потребуется время на восстановление. Никогда не насилуйте себя! Хороший " +
                            "полководец знает, когда нужно отступить. Если чувство дискомфорта стало слишком интенсивным, стоит на время отложить занятие и " +
                            "переключиться на другой вид деятельности. Отдых поможет восстановить потраченные силы и вернуться в ресурсное состояние, " +
                            "в котором работа будет сделана лучше и быстрее.",
                    "1553770511934");
        }
        if (oldVersion < 2) {
            //Код добавления нового столбца
        }
    }

    private static void insertEntry(SQLiteDatabase db, String title,
                                    String content, String date) {
        ContentValues entries = new ContentValues();
        entries.put("TITLE", title);
        entries.put("CONTENT", content);
        entries.put("DATE", date);
        db.insert("ENTRY", null, entries);
    }

    public static List<Entry> getAllEntries(Context context) {
        List<Entry> entriesFromSQL = new ArrayList<>();
        SQLiteOpenHelper entryDatabaseHelper = new EntryDatabaseHelper(context);
        try{
            SQLiteDatabase db = entryDatabaseHelper.getReadableDatabase();
            Cursor cursor = db.query ("ENTRY",
                    new String[] {"_id","TITLE", "CONTENT", "DATE"},
                    null, null, null, null, "DATE ASC");
            if (cursor.moveToFirst()){
                do{
                    int id = cursor.getInt(0);
                    String title = cursor.getString(1);
                    String content = cursor.getString(2);
                    String date = cursor.getString(3);
                    long dt = Long.parseLong(date,10);
                    Entry e = new Entry(id,title,content,dt);
                    entriesFromSQL.add(e);
                }while(cursor.moveToNext());
            }
            cursor.close();
            db.close();
        }
        catch(SQLiteException e) {
            Toast toast = Toast.makeText(context,
                    R.string.db_read_error,
                    Toast.LENGTH_SHORT);
            toast.show();
        }
        entries = entriesFromSQL;
        return entriesFromSQL;
    }

    public static void addNewEntry(Context context, Entry entry) {
        SQLiteOpenHelper entryDatabaseHelper = new EntryDatabaseHelper(context);
        try{
            SQLiteDatabase db = entryDatabaseHelper.getWritableDatabase();
            insertEntry(db,entry.getTitle(),entry.getContent(),Long.toString(entry.getDate()));
            db.close();
        }
        catch(SQLiteException e) {
            Toast toast = Toast.makeText(context,
                    R.string.db_save_error,
                    Toast.LENGTH_SHORT);
            toast.show();
        }
    }
    public static void editEntry(Context context, Entry entry) {
        SQLiteOpenHelper entryDatabaseHelper = new EntryDatabaseHelper(context);
        try{
            SQLiteDatabase db = entryDatabaseHelper.getWritableDatabase();
            //insertEntry(db,entry.getTitle(),entry.getContent(),Long.toString(entry.getDate()));
            ContentValues entries = new ContentValues();
            entries.put("TITLE", entry.getTitle());
            entries.put("CONTENT", entry.getContent());
            entries.put("DATE", Long.toString(entry.getDate()));
            db.update("ENTRY",entries,"_id = ?",new String[] {Long.toString(entry.getId())});
            db.close();
        }
        catch(SQLiteException e) {
            Toast toast = Toast.makeText(context,
                    R.string.db_save_error,
                    Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public static void deleteEntry(Context context, int entryId) {
        SQLiteOpenHelper entryDatabaseHelper = new EntryDatabaseHelper(context);
        try{
            SQLiteDatabase db = entryDatabaseHelper.getWritableDatabase();
            db.delete("ENTRY",
                    "_id = ?",
                    new String[] {Integer.toString(entryId)});
            db.close();
        }
        catch(SQLiteException e) {
            Toast toast = Toast.makeText(context,
                    R.string.db_delete_error,
                    Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public static List<Entry> getEntries(Context context) {
        return entries;
    }

}
