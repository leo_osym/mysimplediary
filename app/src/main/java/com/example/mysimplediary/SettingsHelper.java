package com.example.mysimplediary;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.preference.PreferenceManager;

import java.util.Locale;

public class SettingsHelper {
    public static void setLocale(Context context) {
        Locale locale = new Locale(getLanguage(context));
        Locale.setDefault(locale);

        Resources resources = context.getResources();
        Configuration configuration = new Configuration(resources.getConfiguration());
        if(Build.VERSION.SDK_INT >=17){
            configuration.setLocale(locale);
        } else {
            configuration.locale = locale;
        }
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
    }

    public static void setTheme(Context context){
        context.setTheme(getTheme(context));
    }

    private static String getLanguage(Context context) {
        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(context);
        String defLanguage = SP.getString("languageChooser","1");
        switch (defLanguage){
            case "1": //English
                return "en";
            case "2": //Russian
                return "ru";
            case "3": //Ukrainian
                return "ua";
        }
        return "en";
    }

    private static int getTheme(Context context){
        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(context);
        String defTheme = SP.getString("themeChooser","1");
        int id = 0;
        switch (defTheme){
            case "1":
                id = R.style.AppTheme;
                break;
            case "2":
                id = R.style.AppTheme2;
                break;
            case "3":
                id = R.style.AppTheme3;
                break;
        }
        return id;
    }
}
