package com.example.mysimplediary;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends AppCompatActivity {

    private ArrayList<Integer> itemsToShow;
    private boolean showAllEntries;
    private List<Entry> entries;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        SettingsHelper.setTheme(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        EditText searchField = (EditText) findViewById(R.id.search_field);
        entries = EntryDatabaseHelper.getAllEntries(this);

        showAllEntries = true;
        reloadFragment();

        searchField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.toString().equals("")){
                    showAllEntries = true;
                } else {
                    // perform search
                    showAllEntries = false;
                    ArrayList<Integer> entriesId = new ArrayList<>();
                    for(int i = 0; i<entries.size();i++){
                        if(entries.get(i).Contains(s.toString())){
                            entriesId.add(i);
                        }
                    }
                    itemsToShow = entriesId;
                }
                reloadFragment();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void reloadFragment(){
        Bundle bundle = new Bundle();
        if(showAllEntries){
            bundle.putBoolean("getAllEntries", true);
        }
        else{
            bundle.putBoolean("getAllEntries", false);
            bundle.putIntegerArrayList("ids", itemsToShow);
        }
        FragmentTransaction ft = (FragmentTransaction) getSupportFragmentManager().beginTransaction();
        EntriesFragment entriesFragment = new EntriesFragment();
        entriesFragment.setArguments(bundle);
        ft.replace(R.id.show_entries_fragment, entriesFragment);
        ft.commit();
    }

}
