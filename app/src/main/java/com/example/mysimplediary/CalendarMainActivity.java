package com.example.mysimplediary;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.listeners.OnDayClickListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CalendarMainActivity extends AppCompatActivity {

    private List<Entry> entries;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // because cached language shows
        this.setTitle(R.string.show_calendar);
        SettingsHelper.setTheme(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        entries = EntryDatabaseHelper.getAllEntries(this);

        // Highlight days with existing entries in the CalendarView
        CalendarView calendarView = (CalendarView) findViewById(R.id.simpleCalendarView);
        List<Calendar> selectedDates = new ArrayList<>();
        Calendar cal = Calendar.getInstance();

        for(int id = 0; id <entries.size();id++) {
            cal.setTimeInMillis(entries.get(id).getDate());
            selectedDates.add((Calendar)cal.clone());
        }
        calendarView.setSelectedDates(selectedDates);

        // Process click on the calendar
        List<Calendar> selectedCal = new ArrayList<>();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy"); // compare only by day
        calendarView.setOnDayClickListener(new OnDayClickListener() {
            @Override
            public void onDayClick(EventDay eventDay) {
                //calendarView.setSelectedDates(selectedDates);
                ArrayList<Integer> entriesId = new ArrayList<>();
                Calendar cal = eventDay.getCalendar();
                selectedCal.clear();
                selectedCal.add(cal);
                calendarView.setDisabledDays(selectedCal);
                for(int i =0; i<selectedDates.size();i++){
                    if(df.format(selectedDates.get(i).getTimeInMillis()).equals(df.format(cal.getTimeInMillis()))){
                        entriesId.add(i);
                    }
                }
                reloadFragment(entriesId);
            }
        });
    }

    private void reloadFragment(ArrayList<Integer> items) {

        Bundle bundle = new Bundle();
        bundle.putBoolean("getAllEntries", false);
        bundle.putIntegerArrayList("ids", items);
        FragmentTransaction ft = (FragmentTransaction) getSupportFragmentManager().beginTransaction();
        EntriesFragment entriesFragment = new EntriesFragment();
        entriesFragment.setArguments(bundle);
        ft.replace(R.id.show_entries_fragment, entriesFragment);
        ft.commit();
    }


}
