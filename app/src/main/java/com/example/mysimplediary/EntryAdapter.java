package com.example.mysimplediary;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.*;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

public class EntryAdapter extends RecyclerView.Adapter<EntryAdapter.ViewHolder> {

    private String[] titles;
    private String[] contents;
    private long[] dates;
    //private static int entId;

    private Listener listener;
    interface Listener {
        void onClick(int position);
    }

    public void setListener(Listener listener){
        this.listener = listener;
    }

    public EntryAdapter(String[] titles, String[] contents, long[] dates){
        this.titles = titles;
        this.contents = contents;
        this.dates = dates;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        CardView cv = (CardView) LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.entry_card,viewGroup, false);
        return new ViewHolder(cv);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {

        CardView cardView = viewHolder.cardView;
        TextView titleField = (TextView) cardView.findViewById(R.id.info_title);
        titleField.setText(titles[i]);

        TextView dateField = (TextView) cardView.findViewById(R.id.info_date);
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy, HH:mm");
        dateField.setText(df.format(new Date(dates[i])));

        TextView contentField = (TextView) cardView.findViewById(R.id.info_content);
        contentField.setText(contents[i]);
        //contentField.setText(contents[i].length() > 100
        //        ? contents[i].substring(0,99)
         //       : contents[i]);

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onClick(i);
                }
            }
        });
        cardView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                //menu.setHeaderTitle("Context Menu");
                String[] contextMenu = v.getContext().getResources().getStringArray(R.array.context_menu);
                menu.add(0, i, 0, contextMenu[0]);
                menu.add(1, i, 0, contextMenu[1]);
                //menu.add(0, i, 0, "Change date");
                //v.setBackgroundColor(R.attr.contextMenuColor);
            }
        });
    }

    @Override
    public int getItemCount(){
        return titles.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        //Определить представление для каждого элемента данных
        private CardView cardView;

        public ViewHolder(CardView v) {
            super(v);
            cardView = v;
        }
    }
}
