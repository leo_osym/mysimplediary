package com.example.mysimplediary;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import java.util.Calendar;
import java.util.List;

public class NewEntryActivity extends AppCompatActivity {

    public static final String ENTRY_ID = "entryId";
    public static final int ENTRY_DATE_RESULT = 0;
    private List<Entry> entries;
    private long entryDate = 0;
    private int entryId = -1;
    private EditText et1;
    private EditText et2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        SettingsHelper.setTheme(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_entry);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        entryId = (Integer)getIntent().getExtras().get(ENTRY_ID);

        //entries = Entry.entries;//changes here!
        entries = EntryDatabaseHelper.getAllEntries(this);
        //entries = EntryDatabaseHelper.getEntries(this);

        if(entryId!=-1) {
            et1 = (EditText) findViewById(R.id.new_entry_title);
            et2 = (EditText) findViewById(R.id.new_entry_content);
            et1.setText(entries.get(entryId).getTitle());
            et2.setText(entries.get(entryId).getContent());
            entryDate = entries.get(entryId).getDate();
        }
        else
            entryDate = Calendar.getInstance().getTimeInMillis();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            entryDate = extras.getLong("DATE");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.add_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch(item.getItemId()){
            case R.id.action_save_changes: {
                et1 = (EditText) findViewById(R.id.new_entry_title);
                et2 = (EditText) findViewById(R.id.new_entry_content);
                String title = et1.getText().toString();
                String content = et2.getText().toString();
                //Entry entry = new Entry(-1,title,content, entryDate);
                if(entryId==-1) {
                    //entries.add(new Entry(-1,title,content, entryDate));
                    EntryDatabaseHelper.addNewEntry(this, new Entry(-1,title,content, entryDate));
                }
                else{
                    //entries.set(entryId, new Entry(entries.get(entryId).getId(),title,content, entryDate));
                    EntryDatabaseHelper.editEntry(this, new Entry(entries.get(entryId).getId(),title,content, entryDate));
                }
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                return true;
            }
            case R.id.action_open_calendar: {
                startActivityForResult(new Intent(this, CalendarEditActivity.class), ENTRY_DATE_RESULT);
            }
            default:
                return super.onOptionsItemSelected(item);

        }
    }
}
