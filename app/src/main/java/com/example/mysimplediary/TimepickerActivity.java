package com.example.mysimplediary;

import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TimePicker;

public class TimepickerActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //SettingsHelper.setLocale(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timepicker);

        final TimePicker timePicker = (TimePicker) findViewById(R.id.timePicker);
        timePicker.setIs24HourView(true);

        Button btnSet = (Button) findViewById(R.id.set_date_button);
        btnSet.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int hour, minute;
                if(Build.VERSION.SDK_INT < 23){
                    hour = timePicker.getCurrentHour();
                    minute = timePicker.getCurrentMinute();
                }
                else{
                    hour = timePicker.getHour();
                    minute = timePicker.getMinute();
                }
                Intent intent = new Intent();
                intent.putExtra("HOUR", hour);
                intent.putExtra("MINUTE", minute);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
        Button btnCancel = (Button) findViewById(R.id.cancel_date_button);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("HOUR", 0);
                intent.putExtra("MINUTE", 0);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }

}
