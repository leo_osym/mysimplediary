package com.example.mysimplediary;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

public class GoogleAuthHelper {

    public static GoogleSignInAccount account;
    public static DriveServiceHelper mDriveServiceHelper;
    public static GoogleSignInClient mGoogleSignInClient;
    public static int RC_SIGN_IN = 0;
    public static boolean isSignedIn = false;
    public static boolean isSyncEnabled = false;
}
