package com.example.mysimplediary;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import java.util.List;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private List<Entry> entries;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        SettingsHelper.setLocale(this);
        // because cached language shows
        this.setTitle(R.string.app_main);
        SettingsHelper.setTheme(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        entries = EntryDatabaseHelper.getAllEntries(this);
        //entries = EntryDatabaseHelper.getEntries(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this,
                drawer,
                toolbar,
                R.string.nav_open_drawer,
                R.string.nav_close_drawer);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        View headerLayout = navigationView.getHeaderView(0);
        ImageView userPhoto = headerLayout.findViewById(R.id.user_photo_image);
        TextView userName = headerLayout.findViewById(R.id.user_name_field);

        GoogleAuthHelper.account = GoogleSignIn.getLastSignedInAccount(this);
        if(GoogleAuthHelper.account!=null){
            userPhoto.setVisibility(View.VISIBLE);
            userName.setVisibility(View.VISIBLE);
            Uri photoUri = GoogleAuthHelper.account.getPhotoUrl();
            Glide.with(this)
                    .load(photoUri)
                    .apply(RequestOptions.circleCropTransform())
                    .into(userPhoto);
            userName.setText(GoogleAuthHelper.account.getDisplayName());
            userName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getBaseContext(),SettingsMainActivity.class));
                }
            });

        }

        Bundle bundle = new Bundle();
        bundle.putBoolean("getAllEntries", true);
        FragmentTransaction ft = (FragmentTransaction) getSupportFragmentManager().beginTransaction();
        EntriesFragment entriesFragment = new EntriesFragment();
        entriesFragment.setArguments(bundle);
        ft.replace(R.id.show_entries_fragment, entriesFragment);
        ft.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch(item.getItemId()){
            case R.id.action_create_entry: {
                Intent intent = new Intent(this, NewEntryActivity.class);
                intent.putExtra(NewEntryActivity.ENTRY_ID, -1);
                startActivity(intent);
                return true;
            }
            case R.id.action_open_calendar:{
                Intent intent = new Intent(this, CalendarMainActivity.class);
                startActivity(intent);
                return true;
            }
            case R.id.action_search:{
                Intent intent = new Intent(this, SearchActivity.class);
                startActivity(intent);
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();
        Intent intent = null;

        switch(id){
            case R.id.settings: {
                intent = new Intent(this,SettingsMainActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.sync_entries: {
                Toast.makeText(this, "Sync is not allowed yet!",Toast.LENGTH_SHORT).show();
                break;
            }
            case R.id.about: {
                intent = new Intent(this,AboutActivity.class);
                startActivity(intent);
                break;
            }
        }

        //startActivity(intent);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }



}
