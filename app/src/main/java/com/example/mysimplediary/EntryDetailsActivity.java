package com.example.mysimplediary;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;

public class EntryDetailsActivity extends AppCompatActivity {

    public static final String ENTRY_ID = "entryId";
    private List<Entry> entries;
    private int entryId=-1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //SettingsHelper.setLocale(this);
        SettingsHelper.setTheme(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entry_details);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        entryId = (Integer)getIntent().getExtras().get(ENTRY_ID);

        //entries = Entry.entries;// changes here!
        entries = EntryDatabaseHelper.getAllEntries(this);
        //entries = EntryDatabaseHelper.getEntries(this);

        String title = entries.get(entryId).getTitle();
        TextView titleView = (TextView)findViewById(R.id.title_show);
        titleView.setText(title);
        String content = entries.get(entryId).getContent();
        TextView contentView = (TextView)findViewById(R.id.content_show);
        contentView.setText(content);
        long date = entries.get(entryId).getDate();
        TextView dateView = (TextView)findViewById(R.id.date_show);
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy, HH:mm");
        dateView.setText(df.format(date));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.show_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch(item.getItemId()){
            case R.id.action_edit_entry: {
                Intent intent = new Intent(this, NewEntryActivity.class);
                //int entryId = (Integer)getIntent().getExtras().get(ENTRY_ID);
                intent.putExtra(NewEntryActivity.ENTRY_ID, entryId);
                startActivity(intent);
                return true;
            }
            case R.id.action_delete_entry: {
                new AlertDialog.Builder(this)
                        .setTitle(R.string.delete_dialog_title)
                        .setMessage(R.string.delete_dialog_content)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if (entryId!=-1)EntryDatabaseHelper.deleteEntry(getBaseContext(),
                                        entries.get(entryId).getId());
                                Intent intent = new Intent(getBaseContext(), MainActivity.class);
                                startActivity(intent);
                            }
                        })
                        // A null listener allows the button to dismiss the dialog and take no further action.
                        .setNegativeButton(android.R.string.no, null)
                        // .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
                return true;
            }

            default:
                return super.onOptionsItemSelected(item);

        }
    }
}
