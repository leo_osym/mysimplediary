package com.example.mysimplediary;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

public class SettingsMainActivity extends AppCompatActivity {

    private Button signInButton;
    private SharedPreferences SP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        SettingsHelper.setTheme(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        getFragmentManager().beginTransaction().replace(R.id.preferences_fragment, new MyPreferenceFragment()).commit();

        signInButton = findViewById(R.id.sign_in_button);
        SP = PreferenceManager.getDefaultSharedPreferences(this);

        isSync();
        SP.registerOnSharedPreferenceChangeListener(new SharedPreferences.OnSharedPreferenceChangeListener() {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                isSync();
            }
        });

        GoogleAuthHelper.account = GoogleSignIn.getLastSignedInAccount(this);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        GoogleAuthHelper.mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        //signOut();
        setButtonState();
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!GoogleAuthHelper.isSignedIn){
                    signIn();
                    GoogleAuthHelper.isSignedIn = true;
                    signInButton.setText(R.string.settings_button_sign_out);
                } else {
                    signOut();
                    GoogleAuthHelper.isSignedIn = false;
                    signInButton.setText(R.string.settings_button_sign_in);
                }
            }
        });
    }

    private void setButtonState(){
        if(GoogleAuthHelper.account==null){
            signInButton.setText(R.string.settings_button_sign_in);
            GoogleAuthHelper.isSignedIn = false;
        } else {
            signInButton.setText(R.string.settings_button_sign_out);
            GoogleAuthHelper.isSignedIn = true;
        }
    }

    private void isSync(){
        boolean isSyncEnabled = SP.getBoolean("enableSync", false);
        GoogleAuthHelper.isSyncEnabled = isSyncEnabled;
        if(isSyncEnabled){
            signInButton.setVisibility(View.VISIBLE);
        }
        else{
            signInButton.setVisibility(View.GONE);
            signOut();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == GoogleAuthHelper.RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
            task.addOnSuccessListener(new OnSuccessListener<GoogleSignInAccount>() {
                @Override
                public void onSuccess(GoogleSignInAccount account) {
                    if(GoogleAuthHelper.account != null){
                        Toast.makeText(getBaseContext(),
                                "You signed as "+GoogleAuthHelper.account.getDisplayName() + ", img: "+ GoogleAuthHelper.account.getPhotoUrl(),
                                Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {

        try {
            GoogleSignInAccount acc = completedTask.getResult(ApiException.class);
            // Signed in successfully, show authenticated UI.
            GoogleAuthHelper.account = acc;
        } catch (ApiException e) {
            GoogleAuthHelper.account = null;
        }
    }

    private void signIn(){
        GoogleAuthHelper.account = GoogleSignIn.getLastSignedInAccount(this);
        if(GoogleAuthHelper.account == null)
        {
            Intent signInIntent = GoogleAuthHelper.mGoogleSignInClient.getSignInIntent();
            startActivityForResult(signInIntent, GoogleAuthHelper.RC_SIGN_IN);
        }
    }

    private void signOut(){
        if(GoogleAuthHelper.mGoogleSignInClient != null)
            GoogleAuthHelper.mGoogleSignInClient.signOut();
    }



    public static class MyPreferenceFragment extends PreferenceFragment
    {
        @Override
        public void onCreate(final Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_main);
        }
    }
}
