package com.example.mysimplediary;

import java.util.ArrayList;

public class Entry {
    private int entry_id;
    private String title;
    private String content;
    private long date;

    public Entry(int entry_id, String title, String content, long date){
        this.entry_id = entry_id;
        this.title = title;
        this.content = content;
        this.date = date;
    }

    public int getId(){
        return entry_id;
    }

    public String getTitle(){
        return title;
    }

    public String getContent(){
        return content;
    }

    public long getDate() {return date;}

    public boolean Contains(String str){
        str = str.toLowerCase();
        if(title.toLowerCase().contains(str) | content.toLowerCase().contains(str))
            return true;
        return  false;
    }
}
