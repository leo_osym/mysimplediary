package com.example.mysimplediary;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class CalendarEditActivity extends AppCompatActivity {

    public static final int ENTRY_TIME_RESULT = 0;
    private Calendar calendar;
    private int Year = 0;
    private int Month = 0;
    private int Day = 0;
    private int Hour = 0;
    private int Minute = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar_edit);

        CalendarView simpleCalendarView = (CalendarView) findViewById(R.id.simpleCalendarView); // get the reference of CalendarView
        simpleCalendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                Year = year;
                Month = month;
                Day = dayOfMonth;
                Intent intent = new Intent(view.getContext(), TimepickerActivity.class);
                startActivityForResult(intent, ENTRY_TIME_RESULT);
            }
        });


        Button btnSet = (Button) findViewById(R.id.set_date_button);
        btnSet.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(calendar == null) calendar = Calendar.getInstance();
                Intent intent = new Intent();
                intent.putExtra("DATE", calendar.getTimeInMillis());
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        Button btnCancel = (Button) findViewById(R.id.cancel_date_button);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                setResult(RESULT_CANCELED, intent);
                finish();
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Bundle extras = data.getExtras();
        Hour = extras.getInt("HOUR");
        Minute = extras.getInt("MINUTE");
        calendar = Calendar.getInstance();
        calendar.set(Year, Month, Day, Hour, Minute);

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy, HH:mm");
        String dateText = getResources().getString(R.string.time_hint) + df.format(calendar.getTimeInMillis());
        TextView ex = (TextView)findViewById(R.id.just_show_calendar);
        ex.setText(dateText);
    }
}
